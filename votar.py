from datetime import date
# codigo para descobrir se uma pessoa pode ou não votar no Brasil de acordo com a sua idade.


def votar(ano):
    atualAno = date.today().year
    idade = atualAno - ano
    if idade < 16:
        return f'Não vota! Idade: {idade} anos'
    elif idade >= 16 and idade < 18 or idade > 65:
        return f' Voto não obrigatorio! Idade: {idade} anos'
    else:
        return f' Voto obrigatorio! Idade {idade} anos'


nasc = 2000
print(votar(nasc))
